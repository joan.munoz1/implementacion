import java.util.HashMap;

public class map {

    public static void main(String[] args) {
        HashMap<String, Integer> mapita = new HashMap<>();

        mapita.put("el profe", 1);
        mapita.put("se pone", 2);
        mapita.put("la 10", 3);
        mapita.put("y me pone", 4);
        mapita.put("5", 5);

        System.out.println("El tamaño del mapa es: " + mapita.size());
        System.out.println(mapita);

        if (mapita.containsKey("el profe")) {
            Integer j = mapita.get("el profe");
            System.out.println("el valor de la llave" + " \"el profe\" es:- " + j);
        }
    }
}
